/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package ma.omni.workout.initialdata.constants;

/**
 * Global class for all WorkoutInitialData constants.
 */
public final class WorkoutInitialDataConstants extends GeneratedWorkoutInitialDataConstants
{
	public static final String EXTENSIONNAME = "workoutinitialdata";

	private WorkoutInitialDataConstants()
	{
		//empty
	}
}
