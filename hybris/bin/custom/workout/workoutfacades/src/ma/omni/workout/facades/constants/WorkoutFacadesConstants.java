/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package ma.omni.workout.facades.constants;

/**
 * Global class for all WorkoutFacades constants.
 */
public class WorkoutFacadesConstants extends GeneratedWorkoutFacadesConstants
{
	public static final String EXTENSIONNAME = "workoutfacades";

	private WorkoutFacadesConstants()
	{
		//empty
	}
}
