/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package ma.omni.workout.core.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import ma.omni.workout.core.constants.WorkoutCoreConstants;
import ma.omni.workout.core.setup.CoreSystemSetup;


/**
 * Do not use, please use {@link CoreSystemSetup} instead.
 * 
 */
public class WorkoutCoreManager extends GeneratedWorkoutCoreManager
{
	public static final WorkoutCoreManager getInstance()
	{
		final ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (WorkoutCoreManager) em.getExtension(WorkoutCoreConstants.EXTENSIONNAME);
	}
}
