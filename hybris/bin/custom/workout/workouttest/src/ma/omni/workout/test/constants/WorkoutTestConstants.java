/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package ma.omni.workout.test.constants;

/**
 * 
 */
public class WorkoutTestConstants extends GeneratedWorkoutTestConstants
{

	public static final String EXTENSIONNAME = "workouttest";

}
