/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package ma.omni.workout.fulfilmentprocess.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import ma.omni.workout.fulfilmentprocess.constants.WorkoutFulfilmentProcessConstants;

public class WorkoutFulfilmentProcessManager extends GeneratedWorkoutFulfilmentProcessManager
{
	public static final WorkoutFulfilmentProcessManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (WorkoutFulfilmentProcessManager) em.getExtension(WorkoutFulfilmentProcessConstants.EXTENSIONNAME);
	}
	
}
