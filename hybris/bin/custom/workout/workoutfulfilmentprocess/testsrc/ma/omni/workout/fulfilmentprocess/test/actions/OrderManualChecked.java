/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package ma.omni.workout.fulfilmentprocess.test.actions;

/**
 * Test counterpart for {@link ma.omni.workout.fulfilmentprocess.actions.order.OrderManualCheckedAction}
 */
public class OrderManualChecked extends TestActionTemp
{
	//EMPTY
}
