/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package ma.omni.workout.fulfilmentprocess.test.actions;

import ma.omni.workout.fulfilmentprocess.actions.order.ScheduleForCleanUpAction;


/**
 * Test counterpart for {@link ScheduleForCleanUpAction}
 */
public class ScheduleForCleanUp extends TestActionTemp
{
	//EMPTY
}
